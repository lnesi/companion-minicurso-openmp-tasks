// compile with gcc cholesky_blocos.c -o cholesky_blocos -llapack_atlas -lcblas -fopenmp

#include <stdio.h>
#include <sys/time.h>
#include <cblas-atlas.h>
#include <clapack.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>

#define ELEM(A, an, i, j) (A[(i) * (an) + (j)])
#define INDEX(an, i, j) ((i) * (an) + (j))

#define INDEX_BLOCO(bN, nB, i, j) (((i)*(nB*bN*bN))+(j)*(bN*bN))
#define ELEM_BLOCO(Mat, bN, nB, i, j) (Mat[(((i)*(nB*bN*bN))+(j)*(bN*bN))])

#define LIN_PARA_BLOCO 0
#define BLOCO_PARA_LIN 1

#define TEMPO_DESDE_INICIO ((double)( (fim.tv_sec - inicio.tv_sec) + (fim.tv_usec - inicio.tv_usec)*1e-6 ))

#define MAX_PRIO omp_get_max_task_priority()

double* ler_matriz(char* arquivo, int* N){
  double* matriz = NULL;
  FILE* arq = fopen(arquivo, "r");

  if(arq == NULL){
    printf("Erro ao abrir o arquivo <%s>!\n", arquivo);
    exit(1);
  }

  fscanf(arq, "%d", N); // cabeçalho

  matriz = (double *) malloc(*N * sizeof(double));
  int i = 0;
  while(feof(arq) == 0 && i<*N){
    fscanf(arq, "%lg", &matriz[i++]);
  }
  return(matriz);
}

void lin_para_bloco(double lin_mem[], double bloc_mem[], int matrixN, int blockN, int inverso){

  int c = 0, g = 0;
  int iOrig, jOrig;
  int nBlocks = matrixN/blockN;
  for(int ib = 0; ib < nBlocks; ib++){
    for(int jb = 0; jb < nBlocks; jb++){
      for(int i = 0; i < blockN; i++){
	for(int j = 0; j < blockN; j++){
	  iOrig = ib*blockN + i;
	  jOrig = jb*blockN + j;
	  if(inverso){
	    lin_mem[INDEX(matrixN, iOrig, jOrig)] = bloc_mem[g];
	  }else{
	     bloc_mem[g] = lin_mem[INDEX(matrixN, iOrig, jOrig)];
	  }
	  g++;
	}
      }
    }
  }
}


void potrf(double* MA, int tam){
  clapack_dpotrf(CblasRowMajor, 
		 CblasLower,
		 tam, 
		 MA, 
		 tam);
}

void trsm(double* MA, double* MB, int tam){
  cblas_dtrsm(CblasRowMajor,
	      CblasRight, 
	      CblasLower,
	      CblasTrans,
	      CblasNonUnit, 
	      tam,
	      tam,
	      1.0,
	      MA, 
	      tam,
	      MB, 
	      tam);
}

void syrk(double* MA, double* MC, int tam){
   cblas_dsyrk(CblasRowMajor, 
	      CblasLower,
	      CblasNoTrans,
	      tam,
	      tam,
	      -1.0,
	      MA, 
	      tam, 
	      1.0, 
	      MC, 
	      tam); 
}

void gemm(double* MA, double* MB, double* MC, int tam){
  cblas_dgemm(CblasRowMajor,
	      CblasNoTrans,
	      CblasTrans,
	      tam, 
	      tam, 
	      tam, 
	      -1.0, 
	      MA, 
	      tam,
	      MB, 
	      tam, 
	      1.0,
	      MC, 
	      tam);
}

void cholesky(double *A, int OrdemMatriz, int OrdemBloco){
  int i, j, k, prio;
  int NumBlocos = OrdemMatriz / OrdemBloco, TamBloco = OrdemBloco*OrdemBloco;
  double *Akk, *Aik, *Aii, *Aij, *Ajk;
  {
    {
      for(k = 0; k < NumBlocos; k++) {
        Akk = &ELEM_BLOCO(A, OrdemBloco, NumBlocos, k, k);
        potrf(Akk, OrdemBloco);
        for(i = k+1; i < NumBlocos; i++) {
          Aik = &ELEM_BLOCO(A, OrdemBloco, NumBlocos, i, k);
          trsm(Akk, Aik, OrdemBloco);
        }
        for(i = k+1; i < NumBlocos; i++) {
          Aii = &ELEM_BLOCO(A, OrdemBloco, NumBlocos, i, i);
          Aik = &ELEM_BLOCO(A, OrdemBloco, NumBlocos, i, k);
          syrk(Aik, Aii, OrdemBloco);
          for(j = k+1; j < i; j++) {
            Aij = &ELEM_BLOCO(A, OrdemBloco, NumBlocos, i, j);
            Aik = &ELEM_BLOCO(A, OrdemBloco, NumBlocos, i, k);
            Ajk = &ELEM_BLOCO(A, OrdemBloco, NumBlocos, j, k);
            gemm(Aik, Ajk, Aij, OrdemBloco);
          }
        }
      }
    }
  }
}

int main(int argc, char *argv[]){
  struct timeval inicio, fim;
  
  if(argc != 4){
    printf("Erro, use: %s <arquivo matriz> <ordem matriz> <ordem bloco>\n", argv[0]);
    exit(1);
  }

  int tamanho;  
  double* mat = ler_matriz(argv[1], &tamanho);
  int TAm = atoi(argv[2]);  // ordem da matriz
  int TAb = atoi(argv[3]);  // ordem dos blocos

  printf("A matriz possui %d elementos\n", tamanho);
  if((TAm*TAm) != tamanho){
    printf("Erro, o número de elementos no arquivo (%d) é incompativel com a ordem da matriz (%d)\n", tamanho, TAm);
    exit(1);
  } else if ((TAm%TAb) != 0) {
    printf("Erro, %dx%d não é um tamanho de bloco válido para uma matriz %dx%d\n", TAb, TAb, TAm, TAm);
    exit(1);
  }

  double *por_linha = mat; // lido do arquivo
  double *por_bloco = (double*) malloc(tamanho * sizeof(double)); 
  double *por_linha_resultado = (double*) malloc(tamanho * sizeof(double)); 
 
  lin_para_bloco(por_linha, por_bloco, TAm, TAb, LIN_PARA_BLOCO); // coloca no formato de blocos na mem

  // executa a fatoração por blocos
  gettimeofday(&inicio, 0);
  cholesky(por_bloco, TAm, TAb);
  gettimeofday(&fim, 0);

  lin_para_bloco(por_linha_resultado, por_bloco, TAm, TAb, BLOCO_PARA_LIN); // copia resultado pra linhas pra mostrar

  // comparacao com a chamada sem blocos
  clapack_dpotrf(CblasRowMajor, CblasLower,
		 TAm, 
		 por_linha, 
		 TAm); 
 
  printf("Comparando com o resultado sem blocos...\n");
  double tol = 0.00001, dif;
  int erro = 0;
  for(int m = 0; m < TAm; m++){
   for(int n = 0; n < TAm; n++){
     if( m >= n ){
       dif = fabs(ELEM(por_linha_resultado, TAm, m, n)-ELEM(por_linha, TAm, m, n));
       if(dif > tol){
	 printf("\terro, diferença maior que a tolerancia em %d, %d\n", m, n);
	 erro = 1;
       }
     }
   }
  }
  if(!erro){
    printf("\tverificacao concluida com sucesso!\n");
    printf("%d %d %lf\n", TAm, TAb, TEMPO_DESDE_INICIO);
  }else
    printf("\tverificacao falhou.\n");

  free(mat);
  free(por_bloco);
  free(por_linha_resultado);  
}
