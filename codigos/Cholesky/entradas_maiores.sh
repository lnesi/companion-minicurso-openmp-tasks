#!/bin/bash
# Script para baixar entradas maiores para a Fatoração Cholesky

tudo=0
mat1000=0
mat2000=0
mat5000=0
mat10000=0
mat15000=0
mat20000=0
mat30000=0

zenodo_url="https://zenodo.org/record/4675704"

help_script()
{
    cat << EOF
Uso: $0 opções

Script para baixar e extrair entradas maiores para a Fatoração Cholesky

OPÇÕES:
   -a      Obter entrada mat1000 (zip size: 3.6 MB)
   -b      Obter entrada mat2000 (zip size: 14.5 MB)
   -c      Obter entrada mat5000 (zip size: 90.4 MB)
   -d      Obter entrada mat10000 (zip size: 361.5 MB)
   -e      Obter entrada mat15000 (zip size: 813.3 MB)
   -f      Obter entrada mat20000 (zip size: 1.4 GB)
   -g      Obter entrada mat30000 (zip size: 3.3 GB)
   -t      Obter todas as entradas disponíveis (zip size: 6.0 GB)
   -h      Mostra esta mensagem

EOF
}

# Parsear opcoes
while getopts "abcdefgth" opt; do
    case $opt in
	a)
	    mat1000=1
	    ;;
	b)
	    mat2000=1
	    ;;
	c)
	    mat5000=1
	    ;;
	d)
	    mat10000=1
	    ;;
	e)
	    mat15000=1
	    ;;
	f)
	    mat20000=1
	    ;;
	g)
	    mat30000=1
	    ;;
	t)
	    tudo=1
	    ;;
	h)
	    help_script
	    exit 1
	    ;;
	\?)
	    echo "Invalid option: -$OPTARG"
	    help_script
	    exit 2
	    ;;
    esac
done

for i in {mat1000,mat2000,mat5000,mat10000,mat15000,mat20000,mat30000};
do
    if [[ $tudo == 1 || ${!i} == 1 ]]; then
	wget $zenodo_url/files/$i.zip
	unzip $i.zip && rm $i.zip
    fi
done
