#include <stdio.h>
#include <unistd.h>
#include <omp.h>

void task(int i){
   sleep(i);
}

int main(){
  #pragma omp parallel num_threads(5)
  {//OpenMP inicia várias threads

    #pragma omp single
    {// Este Bloco é executado unicamente por uma thread
       for(int i = 0; i < 10; i++){
          int prio = i < 5 ? 10 : i;
          #pragma omp task priority(prio)
          task(i);//Each task will sleep diferently
       }
    }

  }
}
