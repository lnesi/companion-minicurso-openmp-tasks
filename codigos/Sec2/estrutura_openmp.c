#include <stdio.h>
#include <omp.h>
int main(){
  #pragma omp parallel
  {//OpenMP inicia várias threads
    printf("Bloco paralelo executado por:%d\n", omp_get_thread_num());
    #pragma omp single
    {// Este Bloco é executado unicamente por uma única thread
      printf("Bloco único executado por:%d\n", omp_get_thread_num());
    }
  }
}
