#include <stdio.h>

void minha_tarefa(char* s){
    printf("%s ", s);
}
int main(){
  #pragma omp parallel
  {//OpenMP inicia várias threads
    #pragma omp single
    {// Este Bloco é executado unicamente por uma thread
       #pragma omp task
       minha_tarefa("Olá");
       //#pragma omp taskwait
       #pragma omp task
       minha_tarefa("Mundo");
    }
  }
}
