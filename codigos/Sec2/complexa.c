#include <stdio.h>
#include <unistd.h>
#include <omp.h>

void tarefa_simples(){
   sleep(1);
}
void tarefa_complexa(){
  #pragma omp task
  tarefa_simples();
  #pragma omp task
  tarefa_simples();
  //#pragma omp taskwait
}
int main(){
  #pragma omp parallel
  {//OpenMP inicia várias threads
    #pragma omp single
    {// Este Bloco é executado unicamente por uma thread
       #pragma omp task //untied
       tarefa_complexa();
       #pragma omp task
       tarefa_simples();
    }
  }
}
