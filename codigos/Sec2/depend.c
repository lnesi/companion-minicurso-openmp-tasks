#include <stdio.h>
#include <omp.h>

int main(){
  int A[4], c=0, d=0, result=0;
  #pragma omp parallel
  {
    #pragma omp single
    {
       #pragma omp task depend(inout: A[0:2], A[2:2])
       {
          A[0]=1; A[1]=1; A[2]=2; A[3]=3;
       }
       #pragma omp task depend(in: A[0:2]) depend(out: c)
       {
          c = A[0] * A[1];
       }
       #pragma omp task depend(in: A[2:2]) depend(out: d)
       {
          d = A[2] * A[3];
       }
       #pragma omp task depend(in: c, d)
       {
          result = c + d;
       }
    }
  }
  printf("Resultado:%d\n", result);
}
