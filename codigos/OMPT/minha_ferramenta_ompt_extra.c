#include <stdio.h>
#include <ompt.h>
#include <omp.h>
double INIT_TIME;
unsigned int TASK_ID;

FILE * trace_file;

// Define o comportamento para este callback
static void on_ompt_callback_task_schedule(ompt_data_t *first_task_data, ompt_task_status_t prior_task_status, ompt_data_t *second_task_data){
  switch (prior_task_status) {
    case ompt_task_complete:
      fprintf(trace_file, "%d %ld tarefa_terminou %lf\n", omp_get_thread_num(), first_task_data->value, omp_get_wtime()-INIT_TIME);
      break;
    case ompt_task_switch: // task second_task_data começou
      fprintf(trace_file, "%d %ld tarefa_iniciou %lf\n", omp_get_thread_num(), second_task_data->value, omp_get_wtime()-INIT_TIME);
      break;
    default:
      break;
  }
}

static void on_ompt_callback_task_create( ompt_data_t *parent_task_data,
      const ompt_frame_t *parent_frame, ompt_data_t* new_task_data,
      int flag, int has_dependences, const void *codeptr_ra)
{
  new_task_data->value = TASK_ID++;
  fprintf(trace_file, "%d %ld tarefa_criada %lf\n", omp_get_thread_num(), new_task_data->value, omp_get_wtime()-INIT_TIME);
}

int ompt_initialize(ompt_function_lookup_t lookup, ompt_data_t* data) {
#ifdef USE_RASTRO_FILE
  trace_file = fopen ("rastro.out","w");
#else
  trace_file = stdout;
#endif
  if(trace_file == NULL){
    fprintf(stderr, "Erro ao abrir arquivo de rastro!");
    return 0;
  }
  fprintf(trace_file, "Thread Tarefa Evento Tempo\n");
  fprintf(trace_file, "%d 0 OMPT_LIB_INICIO %lf\n", omp_get_thread_num(), omp_get_wtime()-INIT_TIME);
  // Registra os callbacks que queremos usar
  ompt_set_callback_t ompt_set_callback = (ompt_set_callback_t) lookup("ompt_set_callback");
  ompt_set_callback(ompt_callback_task_schedule, (ompt_callback_t)on_ompt_callback_task_schedule);
  ompt_set_callback(ompt_callback_task_create, (ompt_callback_t)on_ompt_callback_task_create);

  return 1; //successo
}

void ompt_finalize(ompt_data_t* data) {
  fprintf(trace_file, "%d 0 OMPT_LIB_FIM %lf\n", omp_get_thread_num(), omp_get_wtime()-INIT_TIME);
}

ompt_start_tool_result_t* ompt_start_tool( unsigned int omp_version, const char *runtime_version) {
  INIT_TIME = omp_get_wtime();
  TASK_ID = 0;
  static ompt_start_tool_result_t ompt_start_tool_result = {&ompt_initialize, &ompt_finalize};
  return &ompt_start_tool_result;
}
