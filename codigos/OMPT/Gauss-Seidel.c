#include <omp.h>
#include <stdio.h>

/* given a system of equations Ax = b:
 * A is the input matrix
 * b is the solution vector
 * N is the size of A[n][n] and length of b[n] plus the borders (+2)
 * Niter is the number of repetitions of the Gauss-Seidel algorithm over the matrix
 * TS is the tile size
 * ITS is the internal tile size
 */
void gauss_seidel_relaxation_tasks_block_block(double **A, double* b, int N, int Niter, int TS, int ITS)
{
	int iii, jjj, k;
	double  h, h2;
	h = 1.0/(N-3);
	h2 = h*h;

	#pragma omp parallel
	{
		#pragma omp single
		{
			for(k=0; k<Niter; ++k) {
				// loop over blocks
				for (iii=1; iii<N-TS; iii+=TS) {
					for (jjj=1; jjj<N-TS; jjj+=TS) {
						#pragma omp task \
						firstprivate(iii, jjj) \
						depend(in: \
						A[iii+TS:1][jjj:TS], \
						A[iii-1:TS][jjj:TS], \
						A[iii:TS][jjj-1:TS], \
						A[iii:TS][jjj+TS:1]) \
						depend(out: A[iii:TS][jjj:TS])
						{
							// loop inside a block
							for(int ii=iii; ii<iii+TS; ii+=ITS) {
								for(int jj=jjj; jj<jjj+TS; jj+=ITS) {
									// loop inside the inner blocks of a block
									for(int i=ii; i<ii+ITS; ++i) {
										for(int j=jj; j<jj+ITS; ++j) {
											// The Gauss-Seidel smoothing formula for two dimensions
											A[i][j] = 0.25 * (	A[i+1][j] +  	// down
																A[i-1][j] +  	// up
																A[i][j+1] +  	// right
																A[i][j-1] -  	// left
															 	h2*b[i]
															 ); 
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void print_mat(double **A, int N) 
{
	printf("Print matrix A:\n");
	for (int i = 0; i < N; ++i)
	{
		for (int j = 0; j < N; ++j)
		{
			printf("%lf ", A[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

// Init matrix a with 1 or 10 and vector b with 1
void init_A_b(double **A, double* b, int N) 
{
	for (int i = 0; i < N; ++i) {
		for (int j = 0; j < N; ++j) {
			A[i][j] = (j+i) % 2 == 0 ? 1.0 : 10.0;
		}
		b[i] = 1.0;
	}
}

void parameters_check(int N, int Niter, int BS, int IBS) {
	if(N%BS != 0){
		printf("[Parameter Error] BS and N must be multiples!\n");
		exit(1);
	} else if(Niter < 0) {
		printf("[Parameter Error] Niter must be positive!\n");
		exit(1);
	} else if(IBS > BS || BS%IBS != 0) {
		printf("[Parameter Error] IBS and BS must be multiples & IBS <= BS!\n");
		exit(1);
	}
}

int main(int argc, char const *argv[])
{
	// omp_set_num_threads(1);
	int N, Niter, BS, IBS;

	scanf("%d %d %d %d", &N, &Niter, &BS, &IBS);
	printf("%d %d %d %d \n", N, Niter, BS, IBS);
	parameters_check(N, Niter, BS, IBS);
	N=N+2; // Add +2 to make the matrix borders (something like ghost cells)

	// initialize matrix A and rhs b
	double** A;
	double* b;

	A = (double**) malloc((N)*sizeof(double*));
	b = (double*) malloc((N)*sizeof(double));
	
	for(int i=0; i<N; ++i) {
		A[i] = (double*) malloc((N)*sizeof(double));
	}

	init_A_b(A, b, N);
	gauss_seidel_relaxation_tasks_block_block(A, b, N, Niter, BS, IBS);
	// print_mat(A, N);

	return 0;
}